namespace _20180228_examen01_24933
{
    public class Transaction
    {
        internal Client sender;
        internal Client receiver;
        internal double quantity;
        internal IFormatProvider formatter;

        public Transaction(Client sender, Client receiver, double quantity) 
        {
            this.sender = sender;
            this.receiver = receiver;
            this.quantity = quantity;
            formatter = new NullFormatProvider();
        }

        
    }
}