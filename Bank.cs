namespace _20180228_examen01_24933 
{
    public abstract class Bank
    {
        public IProtocol protocol;

        public abstract Client newClient(string name);
        public abstract void receive(string data, IFormatProvider formatProvider);
    }
}