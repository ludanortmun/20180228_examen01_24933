using System;

namespace _20180228_examen01_24933
{
    public class Client
    {
        public Guid Id {
            get;
        }
        public string Name {
            get;
        }

        public Bank bank {
            get;
        }

        public Client(string name, Bank bank) 
        {
            Name = name;
            this.bank = bank;
            Id = new Guid();
        }

        public Client(string name, string id) 
        {
            Name = name;
            Id = new Guid(id);
        }

    }
}