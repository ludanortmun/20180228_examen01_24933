namespace _20180228_examen01_24933
{
    public interface IProtocol
    {
        void send(Transaction transaction);
        Transaction receive(string data, IFormatProvider format);
    }
}