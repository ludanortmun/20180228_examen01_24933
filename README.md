# 20180228_EXAMEN01_24933
La clase abstracta Bank es el suertipo para los bancos, y asegura que todos tengan protocolo y la habilidad de recibir. Cada implementación abstracta decide cómo leerlo. Puede llamar a receive para imprimir el mensaje de que recibió transacción; en dicho método llama al método receive de su protocolo.

Client sólo asocia clientes con su banco y su información necesaria.

IFormatProvider es el supertipo de los formatproviders y asegura que cada uno pueda hacerlo como necesite. Sus implementaciones concretas lo hacen como necesiten. Codifica transacciones a un string y decodifica strings a transacciones.

IProtocol se asegura de enviar y recibir, lo cual es utilizado por el banco (Bank has a protocol instead of Bank is a protocol). Cada implementación concreta lo hace como necesita. Send envía el string formado por el format provider de la transacción. receive 

Transaction es el supertipo genérico para las transacciones, incluyendo los datos necesarios para la interoperabilidad entre bancos. Se asegura de que también incluya cada uno un Format Provider.
Las implementaciones concretas son decoradores de la transacción base que el asigna funcionalidad nueva, dada como los parámetros que cada banco requiere.