namespace _20180228_examen01_24933
{
    public class NullFormatProvider : IFormatProvider
    {
        public string Encode(Transaction transaction) {
            throw new System.Exception("No format provider specified");
        }

        public Transaction Decode(string data)
        {
            throw new System.Exception("No format provider specified");
        }
    }
}