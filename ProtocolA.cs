namespace _20180228_examen01_24933
{
    public class ProtocolA : IProtocol
    {
        public void send(Transaction transaction) 
        {
            Client receiver = transaction.receiver;
            receiver.bank.receive(transaction.formatter.Encode(transaction), transaction.formatter);
        }

        public Transaction receive(string data, IFormatProvider format)
        {
            return format.Decode(data);
        }
    }
}