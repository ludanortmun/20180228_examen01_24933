using System;
namespace _20180228_examen01_24933
{
    public class JSONFormatProvider : IFormatProvider
    {
        public string Encode(Transaction transaction) {
            Console.WriteLine("Formateando en JSON");
            //No es formato JSON en realidad
            return string.Format("{0}, {1}, {2}", transaction.receiver.ToString(), transaction.sender.ToString(), transaction.quantity);
        }

        public Transaction Decode(string data)
        {
            //Crear nuevo transaction a partir del data obtenido.
            throw new System.Exception("No format provider specified");
        }
    }
}