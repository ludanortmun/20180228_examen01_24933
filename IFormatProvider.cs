namespace _20180228_examen01_24933
{
    public interface IFormatProvider
    {
        string Encode(Transaction transaction);
        Transaction Decode(string data);
    }
}